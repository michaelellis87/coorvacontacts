package com.teamandroid.mike.coorvacontacts

import android.content.Intent
import android.os.Bundle
import com.f2prateek.dart.HensonNavigable
import com.xmartlabs.bigbang.ui.BaseAppCompatActivity

@HensonNavigable
class StartActivity : BaseAppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val intent = Henson.with(context)
        .gotoWelcomeActivity()
        .build()

    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION or Intent.FLAG_ACTIVITY_CLEAR_TASK))
  }
}
