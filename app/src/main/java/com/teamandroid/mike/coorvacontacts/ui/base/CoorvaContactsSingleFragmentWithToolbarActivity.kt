package com.teamandroid.mike.coorvacontacts.ui.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import com.teamandroid.mike.coorvacontacts.R
import com.xmartlabs.bigbang.ui.SingleFragmentActivity
import kotlinx.android.synthetic.main.activity_single_fragment_with_toolbar.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by mike on 8/5/17.
 */
abstract class CoorvaContactsSingleFragmentWithToolbarActivity : SingleFragmentActivity() {
  override val layoutResId = R.layout.activity_single_fragment_with_toolbar

  @ColorRes
  protected open val toolbarBackgroundColor: Int = R.color.colorPrimaryDark
  @DrawableRes
  protected open val toolbarBackButtonDrawable: Int? = R.drawable.ic_arrow_back_white
  @ColorRes
  protected open val toolbarTitleTextColor: Int = R.color.white
  //Add an icon to be displayed at the end of the toolbar
  @DrawableRes
  protected open val toolbarEndButtonDrawable: Int? = null

  protected open val canGoBack = true

  override fun attachBaseContext(newBase: Context?) = super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setupToolbar()
  }

  private fun setupToolbar() {
    setSupportActionBar(toolbar)
    toolbar.setBackgroundColor(ContextCompat.getColor(context, toolbarBackgroundColor))
    toolbar.setTitleTextColor(ContextCompat.getColor(context, toolbarTitleTextColor))

    if (canGoBack) {
      toolbarBackButtonDrawable?.let {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(it)
      }
    }

    setToolbarTitle(supportActionBar?.title.toString())

    toolbarEndButtonDrawable?.let {
      actionEndIcon.setImageDrawable(ContextCompat.getDrawable(context, it))
      actionEndIcon.setOnClickListener { onActionEndClicked() }
    }
  }

  protected open fun onActionEndClicked() {
    //Override if need an action when actionEndIcon is clicked
  }

  override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
    android.R.id.home -> finish().let { true }
    else -> super.onOptionsItemSelected(item)
  }

  private fun setToolbarTitle(title: String) = supportActionBar?.let {
    it.title = ""
    toolbarTitle.text = title
  }
}
