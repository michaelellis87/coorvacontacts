package com.teamandroid.mike.coorvacontacts.di

import com.teamandroid.mike.coorvacontacts.ui.profile.UserProfileFragment
import com.teamandroid.mike.coorvacontacts.ui.welcome.WelcomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by mike on 08/06/2018
 */
@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
  @ContributesAndroidInjector
  abstract fun contributeWelcomeFragment(): WelcomeFragment

  @ContributesAndroidInjector
  abstract fun contributeUserProfileFragment(): UserProfileFragment
}