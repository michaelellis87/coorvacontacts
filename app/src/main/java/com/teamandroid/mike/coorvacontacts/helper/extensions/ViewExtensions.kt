package com.teamandroid.mike.coorvacontacts.helper.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.teamandroid.mike.coorvacontacts.App

/**
 * Created by mike on 24/05/2018
 */
fun View.visibleIf(condition: Boolean) = let { it.visibility = if (condition) View.VISIBLE else View.GONE }

fun View.visible() = run { visibility = View.VISIBLE }

fun View.invisible() = run { visibility = View.INVISIBLE }

fun View.gone() = run { visibility = View.GONE }

fun View.showKeyboard() {
  val inputMethodManager = App.context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
  inputMethodManager?.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun View.hideKeyboard() {
  val inputMethodManager = App.context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
  inputMethodManager?.hideSoftInputFromWindow(this.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
  clearFocus()
}