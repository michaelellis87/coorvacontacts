package com.teamandroid.mike.coorvacontacts.helper.subscribers

import android.support.annotation.CheckResult
import com.google.gson.Gson
import com.teamandroid.mike.coorvacontacts.App
import com.teamandroid.mike.coorvacontacts.R
import com.teamandroid.mike.coorvacontacts.service.error.CoorvaContactsError
import com.teamandroid.mike.coorvacontacts.ui.base.CoorvaContactsFragment
import com.xmartlabs.bigbang.core.extensions.ignoreException
import com.xmartlabs.bigbang.core.extensions.orDo
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.lang.ref.WeakReference

/**
 * Created by mike on 22/06/2018
 */

open class GeneralSingleSubscriber<T> constructor(dailyMotionFragment: CoorvaContactsFragment? = null)
  : SingleObserver<T> {
  companion object {
    private var gson = Gson()
  }

  private val viewReference = WeakReference<CoorvaContactsFragment>(dailyMotionFragment)

  override fun onSubscribe(disposable: Disposable) = showLoading(true)

  protected open fun showLoading(show: Boolean) {
    viewReference.get()?.let {
      if (it.isViewAlive && showLoadingScreen()) {
        it.showLoading(show)
      }
    }
  }

  override fun onError(throwable: Throwable) {
    val view = viewReference.get()
    if (alertOnError(throwable) && view != null && view.isViewAlive) {
      getErrorFromException(throwable).run {
        view.showError(throwable, this?.error.orDo { App.context.getString(R.string.oops) }, this?.errorDescription.orEmpty())
      }
    }
    showLoading(false)
  }

  protected open fun getErrorFromException(throwable: Throwable) = (throwable as? HttpException)?.ignoreException {
    let { gson.fromJson(it.response().errorBody()?.toString().orEmpty(), CoorvaContactsError::class.java) }
  }

  override fun onSuccess(t: T) = showLoading(false)

  @CheckResult
  protected open fun alertOnError(throwable: Throwable) = true

  @CheckResult
  protected open fun showLoadingScreen() = false
}
