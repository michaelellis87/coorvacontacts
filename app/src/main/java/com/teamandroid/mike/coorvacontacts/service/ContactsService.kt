package com.teamandroid.mike.coorvacontacts.service

import com.teamandroid.mike.coorvacontacts.vo.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by mike on 25/06/2018
 */
interface ContactsService {
  companion object {
    const val PATH_CONTACTS = "contacts"
    const val QUERY_SEARCH = "$PATH_CONTACTS/search/"
    const val ARG_FIRST_NAME = "first_name"
    const val ARG_LAST_NAME = "last_name"
    const val ARG_EMAIL = "email"
  }

  @GET(PATH_CONTACTS)
  fun getContacts(): Single<List<User>>

  @GET(QUERY_SEARCH)
  fun searchContactsByName(@Query(ARG_FIRST_NAME) firstName: String?): Single<List<User>>

  @GET(QUERY_SEARCH)
  fun searchContactsByLastName(@Query(ARG_LAST_NAME) lastName: String?): Single<List<User>>

  @GET(QUERY_SEARCH)
  fun searchContactsByEmail(@Query(ARG_EMAIL) email: String?): Single<List<User>>
}
