package com.teamandroid.mike.coorvacontacts.di

import com.teamandroid.mike.coorvacontacts.service.ContactsService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by mike on 08/06/2018
 */
@Module
class RestServiceModuleApi {
  @Provides
  @Singleton
  internal fun provideVideoService(retrofit: Retrofit) = retrofit.create(ContactsService::class.java)
}