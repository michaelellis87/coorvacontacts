package com.teamandroid.mike.coorvacontacts.di

import android.app.Application
import com.teamandroid.mike.coorvacontacts.App
import com.teamandroid.mike.coorvacontacts.helper.common.BuildInfo
import com.xmartlabs.bigbang.core.module.AndroidModule
import com.xmartlabs.bigbang.core.module.GsonModule
import com.xmartlabs.bigbang.retrofit.module.ServiceGsonModule
import com.xmartlabs.bigbang.retrofit.module.RestServiceModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by mike on 24/05/2018
 */
@Singleton
@Component(
    modules = [
      ActivityModule::class,
      AndroidInjectionModule::class,
      AndroidModule::class,
      AppModule::class,
      ControllerModule::class,
      DbModule::class,
      GsonModule::class,
      OkHttpModule::class,
      RestServiceModule::class,
      RestServiceModuleApi::class,
      ServiceGsonModule::class,
      ViewModelModule::class
    ]
)
interface AppComponent {
  @Component.Builder
  interface Builder {
    @BindsInstance
    fun application(application: Application): Builder

    @BindsInstance
    fun buildInfo(buildInfo: BuildInfo): Builder

    fun restServiceGsonModule(serviceGsonModule: ServiceGsonModule): Builder

    fun restServiceModule(restService: RestServiceModule): Builder

    fun okHttpModule(okHttpModule: OkHttpModule): Builder

    fun build(): AppComponent
  }

  fun inject(app: App)
}