package com.teamandroid.mike.coorvacontacts.service

import com.teamandroid.mike.coorvacontacts.controller.Session
import io.reactivex.Single
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by mike on 12/07/2018
 */
interface AuthService {
  companion object {
    const val URL_ACCESS_TOKEN = "oauth/token/"
    const val HEADER_GET_ACCESS_TOKEN = "Content-Type: application/x-www-form-urlencoded"
  }

  //Access token request
  @Headers(HEADER_GET_ACCESS_TOKEN)
  @POST(URL_ACCESS_TOKEN)
  fun getAccessToken(): Single<Session>
}