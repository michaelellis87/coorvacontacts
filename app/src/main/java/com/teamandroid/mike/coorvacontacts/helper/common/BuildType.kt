package com.teamandroid.mike.coorvacontacts.helper.common

import java.util.*

/**
 * Created by mike on 08/06/2018
 */
enum class BuildType {
  STAGING,
  PRODUCTION,
  ;

  override fun toString() = name.toLowerCase(Locale.getDefault())
}
