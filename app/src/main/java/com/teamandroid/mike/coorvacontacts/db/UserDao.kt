package com.teamandroid.mike.coorvacontacts.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.teamandroid.mike.coorvacontacts.vo.User
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by mike on 24/05/2018
 */
@Dao
interface UserDao {
  /**
   * Get a user by id.

   * @return the user from the table with a specific id.
   */
  @Query("SELECT * FROM User WHERE id = :id")
  fun getUserById(id: String): Flowable<User>

  /** Get sorted users
   * @return sorted users from db and observe its changes **/
  @Query("SELECT * FROM User ORDER BY firstName, lastName, email")
  fun getSortedUsersByName(): Flowable<List<User>>

  /** Get saved sorted users by last name **/
  @Query("SELECT * FROM User ORDER BY lastName, firstName, email")
  fun getSortedUsersByLastName(): Single<List<User>>

  /** Get saved sorted users by email **/
  @Query("SELECT * FROM User ORDER BY email, firstName, lastName")
  fun getSortedUsersByEmail(): Single<List<User>>

  /**
   * Insert a user in the database. If the user already exists, replace it.

   * @param user the user to be inserted.
   */
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertUser(user: User)

  /**
   * Delete all users.
   */
  @Query("DELETE FROM User")
  fun deleteAllUsers()
}