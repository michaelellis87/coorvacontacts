package com.teamandroid.mike.coorvacontacts.ui.welcome

import android.view.View
import com.teamandroid.mike.coorvacontacts.R
import com.teamandroid.mike.coorvacontacts.helper.extensions.load
import com.teamandroid.mike.coorvacontacts.vo.User
import com.xmartlabs.bigbang.core.helper.ui.CircleTransform
import com.xmartlabs.bigbang.ui.common.recyclerview.SingleItemBaseViewHolder
import kotlinx.android.synthetic.main.item_contact.view.*

/**
 * Created by mike on 12/07/2018
 */
class ContactItemView(
    view: View,
    clickListener: ((User) -> Unit)?
) : SingleItemBaseViewHolder<User>(view, clickListener) {
  override fun bindItem(item: User) {
    super.bindItem(item)

    item.image?.let {
      loadAvatar(it)
    }

    itemView.contact_name.text = item.firstName
    itemView.contact_last_name.text = item.lastName
    itemView.contact_email.text = item.email
  }

  private fun loadAvatar(url: String) =
      itemView.user_thumbnail.load(url, R.drawable.ic_android_coorva_24dp) { transform(CircleTransform()) }
}