package com.teamandroid.mike.coorvacontacts.ui.welcome

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.teamandroid.mike.coorvacontacts.Henson
import com.teamandroid.mike.coorvacontacts.R
import com.teamandroid.mike.coorvacontacts.ui.base.CoorvaContactsFragment
import com.teamandroid.mike.coorvacontacts.ui.common.EmptyOnItemSelectedListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_welcome.*
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by mike on 23/05/2018
 */
@FragmentWithArgs
class WelcomeFragment : CoorvaContactsFragment(), SwipeRefreshLayout.OnRefreshListener {
  @Inject
  lateinit var viewModel: CoorvaContactsViewModel

  override val layoutResId = R.layout.fragment_welcome

  private var adapter: ContactsRecyclerAdapter? = null
  private val options = UserOption.values()
  private var searchBySelected = UserOption.NAME
  private var optionSelected = UserOption.NAME
  private val disposable = CompositeDisposable()
  private var latestSearchedText = ""

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setupContactsRecyclerView()
    setupButtons()
  }

  override fun onStart() {
    super.onStart()
    // Subscribe to the emissions of usersList
    // Update the recycler accordingly.
    // In case of error, shows a toast.
    disposable.add(viewModel.observeUsers()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          adapter?.setCustomersSorted(it, optionSelected)
        },
            { error ->
              run {
                Timber.d(error)
                Toast.makeText(activity, getString(R.string.unable_to_get_contacts),
                    Toast.LENGTH_SHORT).show()
              }
            }))
  }

  override fun onStop() {
    super.onStop()
    // clear all the subscription
    disposable.clear()
  }

  private fun setupButtons() {
    setupSearchButton()
    setupSortingByButton()
    setupSearchByButton()
  }

  private fun setupSearchByButton() {
    val searchByAdapter = ArrayAdapter<UserOption>(activity, android.R.layout.simple_spinner_dropdown_item,
        options)

    search_spinner.adapter = searchByAdapter
    search_spinner.onItemSelectedListener = object : EmptyOnItemSelectedListener() {
      override fun onItemSelected(adapter: AdapterView<*>?, view: View?, index: Int, value: Long) {
        handleSearchBySpinnerSelected(options[index])
      }
    }
  }

  private fun handleSearchBySpinnerSelected(userOption: UserOption) {
    searchBySelected = userOption
  }

  private fun setupSortingByButton() {
    val sortingAdapter = ArrayAdapter<UserOption>(activity, android.R.layout.simple_spinner_dropdown_item,
        options)

    sorting_spinner.adapter = sortingAdapter
    sorting_spinner.onItemSelectedListener = object : EmptyOnItemSelectedListener() {
      override fun onItemSelected(adapter: AdapterView<*>?, view: View?, index: Int, value: Long) {
        handleSortByOptionSelected(options[index])
      }
    }
  }

  private fun setupSearchButton() {
    search_button.setOnClickListener { handleSearchButtonPressed() }
  }

  private fun handleSearchButtonPressed() {
    latestSearchedText = search_input.text
    executeCorrectSearch()
  }

  private fun executeCorrectSearch() {
    viewModel.searchBy(latestSearchedText, searchBySelected)
        .subscribe({
          contacts_refresh_layout.isRefreshing = false
          Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show()
        },
            { error ->
              contacts_refresh_layout.isRefreshing = false
              Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
            })
  }

  private fun handleSortByOptionSelected(userOption: UserOption) {
    optionSelected = userOption
    adapter?.sortCurrentCustomers(userOption)
  }

  private fun setupContactsRecyclerView() {
    contacts_empty_view.visibility = View.INVISIBLE
    contacts_empty_view.text = getString(R.string.no_contacts_available)

    contacts_refresh_layout.visibility = View.VISIBLE
    contacts_refresh_layout.setOnRefreshListener(this)
    contacts_recycler_view.layoutManager = LinearLayoutManager(activity)
    this.adapter = ContactsRecyclerAdapter {
      val intent = Henson.with(context)
          .gotoUserProfileActivity()
          .compId(it.id)
          .build()

      startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION or Intent.FLAG_ACTIVITY_CLEAR_TASK))
    }
    contacts_recycler_view.adapter = this.adapter
  }


  override fun onRefresh() {
    viewModel.fetchUsers()
        .subscribe({
          contacts_refresh_layout.isRefreshing = false
          Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show()
        },
            { error ->
              contacts_refresh_layout.isRefreshing = false
              Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
            })
  }
}
