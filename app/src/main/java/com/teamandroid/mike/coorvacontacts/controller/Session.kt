package com.teamandroid.mike.coorvacontacts.controller

import com.xmartlabs.bigbang.core.model.SessionType

/**
 * Created by mike on 08/06/2018
 */
class Session(override var accessToken: String? = null,
              internal var username: String? = null,
              internal var refreshToken: String? = null) : SessionType
