package com.teamandroid.mike.coorvacontacts.repository.user

import com.teamandroid.mike.coorvacontacts.db.UserDao
import com.teamandroid.mike.coorvacontacts.service.ContactsService

import com.teamandroid.mike.coorvacontacts.ui.welcome.UserOption
import com.teamandroid.mike.coorvacontacts.vo.User
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by mike on 29/06/2018
 */
@Singleton
class UserRepository @Inject constructor(
    private val contactsService: ContactsService,
    private val userDao: UserDao
) {

  fun fetchContacts(): Completable {
    return contactsService.getContacts()
        .subscribeOn(Schedulers.io())
        .map { contacts -> contacts.forEach { contact -> userDao.insertUser(contact) } }
        .toCompletable()
  }

  fun observeFetchedContacts(): Flowable<List<User>> {
    return userDao.getSortedUsersByName()
  }

  fun searchContacts(input: String, userOption: UserOption): Completable {
    val searchContactsByName = when (userOption) {
      UserOption.NAME -> contactsService.searchContactsByName(input)
      UserOption.LAST_NAME -> contactsService.searchContactsByLastName(input)
      UserOption.EMAIL -> contactsService.searchContactsByEmail(input)
    }
    return searchContactsByName
        .subscribeOn(Schedulers.io())
        .doOnSuccess { _ ->  userDao.deleteAllUsers()}
        .doOnSuccess { contacts ->
          run {
            contacts.forEach { contact -> userDao.insertUser(contact) }
          }
        }
        .toCompletable()
  }

  fun getUserFromDB(userId: String): Flowable<User> {
    return userDao.getUserById(userId)
  }
}
