package com.teamandroid.mike.coorvacontacts.vo

import android.arch.persistence.room.Entity

/**
 * Created by mike on 27/06/2018
 */
@Entity(primaryKeys = ["query"])
class VideoSearch(
    val query: String,
    val total: Long,
    val has_more: Boolean,
    val page: Long,
    val list: List<Video>)