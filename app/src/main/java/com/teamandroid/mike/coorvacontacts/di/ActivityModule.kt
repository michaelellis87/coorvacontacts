package com.teamandroid.mike.coorvacontacts.di

import com.teamandroid.mike.coorvacontacts.StartActivity
import com.teamandroid.mike.coorvacontacts.ui.profile.UserProfileActivity
import com.teamandroid.mike.coorvacontacts.ui.welcome.WelcomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by mike on 08/06/2018
 */
@Suppress("unused")
@Module
abstract class ActivityModule {
  @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
  abstract fun contributeStartActivity(): StartActivity

  @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
  abstract fun contributeWelcomeActivity(): WelcomeActivity

  @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
  abstract fun contributeUserProfileActivity(): UserProfileActivity
}