package com.teamandroid.mike.coorvacontacts.utils

/**
 * Created by mike on 12/07/2018
 * A functional interface (callback) that accepts a single value.
 *
 * * @param <T> the value type
 */
interface Consumer<T> {
  /**
   * Consume the given value.
   *
   * @param t the value
   */
  fun accept(t: T)
}