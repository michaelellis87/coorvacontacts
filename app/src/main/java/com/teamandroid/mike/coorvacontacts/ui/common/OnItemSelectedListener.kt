package com.teamandroid.mike.coorvacontacts.ui.common

import android.view.View
import android.widget.AdapterView

/**
 * Created by mike on 12/07/2018
 */
open class EmptyOnItemSelectedListener : AdapterView.OnItemSelectedListener {
  override fun onNothingSelected(p0: AdapterView<*>?) { }

  override fun onItemSelected(adapter: AdapterView<*>?, view: View?, index: Int, value: Long) { }
}