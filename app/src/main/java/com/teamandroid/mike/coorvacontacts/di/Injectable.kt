package com.teamandroid.mike.coorvacontacts.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable