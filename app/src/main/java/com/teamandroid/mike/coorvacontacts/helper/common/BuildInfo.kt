package com.teamandroid.mike.coorvacontacts.helper.common

import com.teamandroid.mike.coorvacontacts.BuildConfig
import com.xmartlabs.bigbang.core.model.BuildInfo

/**
 * Created by mike on 08/06/2018
 */
class BuildInfo : BuildInfo {
  override val isDebug = BuildConfig.DEBUG
  override val isProduction: Boolean
    get() = BuildConfig.FLAVOR == BuildType.STAGING.toString()
  override val isStaging: Boolean
    get() = BuildConfig.FLAVOR == BuildType.PRODUCTION.toString()
}