package com.teamandroid.mike.coorvacontacts.ui.profile

import com.f2prateek.dart.InjectExtra
import com.teamandroid.mike.coorvacontacts.ui.base.CoorvaContactsSingleFragmentWithToolbarActivity
import com.xmartlabs.bigbang.ui.BaseFragment

/**
 * Created by mike on 12/07/2018
 */
class UserProfileActivity : CoorvaContactsSingleFragmentWithToolbarActivity() {
  @InjectExtra
  lateinit var compId: String

  override fun createFragment(): BaseFragment = UserProfileFragmentBuilder(compId).build()
}