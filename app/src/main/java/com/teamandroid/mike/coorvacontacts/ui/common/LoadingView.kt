package com.teamandroid.mike.coorvacontacts.ui.common

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.teamandroid.mike.coorvacontacts.R

/**
 * Created by mike on 23/05/2018
 */
class LoadingView(context: Context) : Dialog(context) {
  val view: View = LayoutInflater.from(context).inflate(R.layout.view_loading, null)

  init {
    setCancelable(false)

    val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    addContentView(view, params)

    window?.setBackgroundDrawableResource(android.R.color.transparent)
  }
}