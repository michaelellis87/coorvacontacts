package com.teamandroid.mike.coorvacontacts.ui.common.customview

import android.content.Context
import android.graphics.PorterDuff
import android.support.annotation.DrawableRes
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import com.teamandroid.mike.coorvacontacts.R
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.view_text_input_field.view.*
import org.threeten.bp.Duration
import java.util.*

/**
 * Created by mike on 24/05/2018
 */
class TextInputField @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : TextInputLayout(context, attrs, defStyleAttr), TextWatcher {
  companion object {
    const val NOT_DEFINED = -1
    val SEARCH_THRESHOLD: Duration = Duration.ofMillis(400)
    val MINOR_DELAY: Duration = Duration.ofMillis(50)
    const val SEARCH_MINOR_LENGTH = 2
  }

  val view: View = LayoutInflater.from(context).inflate(R.layout.view_text_input_field, this,
      true)

  private var timer: Timer? = null
  var textValue = ""
  var hintText = ""
  var inputType = NOT_DEFINED
  var imeOptions = NOT_DEFINED
  var maxLength = NOT_DEFINED
  var textColor = NOT_DEFINED
  var hintTitleTextAppearance = NOT_DEFINED
  var textColorHint = NOT_DEFINED
  var underlineColor = NOT_DEFINED
  var searchConsumer: Consumer<String>? = null
  var text: String
    get() = view.editText?.text.toString()
    set(value) {
      view.editText?.setText(value)
    }

  init {
    bindAttributes(context, attrs)
    initializeView()
  }

  private fun initializeView() {
    text = ""

    if (hintTitleTextAppearance != NOT_DEFINED) {
      setHintTextAppearance(hintTitleTextAppearance)
    } else {
      setHintTextAppearance(R.style.TextAppearance_TextPrimaryColor)
    }

    hint = hintText

    if (imeOptions != NOT_DEFINED) {
      view.editText?.imeOptions = imeOptions
    }

    if (inputType != NOT_DEFINED) {
      view.editText?.inputType = inputType
    }

    if (maxLength != NOT_DEFINED) {
      view.editText?.filters = arrayOf(InputFilter.LengthFilter(maxLength))
    }

    if (textColor != NOT_DEFINED) {
      view.editText?.setTextColor(textColor)
    }

    if (textColorHint != NOT_DEFINED) {
      view.editText?.setHintTextColor(textColorHint)
    }

    if (underlineColor != NOT_DEFINED) {
      view.editText
          .postDelayed({
            view.editText.background.mutate().setColorFilter(underlineColor, PorterDuff.Mode.SRC_IN)
          }, MINOR_DELAY.toMillis())
    }

    view.editText
        .postDelayed({
          searchConsumer?.let { view.editText.addTextChangedListener(this) }
        }, MINOR_DELAY.toMillis())

    text = textValue
  }

  private fun bindAttributes(context: Context, attrs: AttributeSet) {
    val a = context.obtainStyledAttributes(attrs, R.styleable.TextInputField, 0, 0)
    textValue = a.getString(R.styleable.TextInputField_android_text).orEmpty()
    hintText = a.getString(R.styleable.TextInputField_android_hint).orEmpty()
    inputType = a.getInt(R.styleable.TextInputField_android_inputType, NOT_DEFINED)
    imeOptions = a.getInt(R.styleable.TextInputField_android_imeOptions, NOT_DEFINED)
    maxLength = a.getInt(R.styleable.TextInputField_android_maxLength, NOT_DEFINED)
    maxLength = a.getInt(R.styleable.TextInputField_android_maxLength, NOT_DEFINED)
    textColor = a.getColor(R.styleable.TextInputField_android_textColor, NOT_DEFINED)
    textColorHint = a.getColor(R.styleable.TextInputField_android_textColorHint, NOT_DEFINED)
    underlineColor = a.getColor(R.styleable.TextInputField_underLineColor, NOT_DEFINED)
    hintTitleTextAppearance = a.getResourceId(R.styleable.TextInputField_hintTextAppearance,
        NOT_DEFINED)
    a.recycle()
  }

  fun addTextChangedListener(watcher: TextWatcher) = view.editText?.addTextChangedListener(watcher)
      ?: Unit

  fun setRightDrawable(@DrawableRes drawable: Int, visible: Boolean) =
      view.editText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, if (visible) drawable else 0, 0)

  override fun toString() = text

  fun linkEnterToAction(action: () -> Unit) {
    view.editText.imeOptions = EditorInfo.IME_ACTION_DONE
    view.editText.setOnEditorActionListener { _, actionId, _ ->
      if (actionId == EditorInfo.IME_ACTION_DONE) {
        action()
      }
      true
    }
  }

  override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    timer?.cancel()
  }

  override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

  override fun afterTextChanged(editable: Editable?) {
    timer = Timer()
    val textToSearch = editable.toString()
    timer?.schedule(object : TimerTask() {
      override fun run() {
        if (textToSearch.length > SEARCH_MINOR_LENGTH) {
          searchConsumer?.accept(textToSearch)
        }
      }
    }, SEARCH_THRESHOLD.toMillis())
  }
}
