package com.teamandroid.mike.coorvacontacts.di

import android.content.Context
import com.teamandroid.mike.coorvacontacts.App
import com.teamandroid.mike.coorvacontacts.R
import com.xmartlabs.bigbang.retrofit.module.RestServiceModule as CoreRestServiceModule
import okhttp3.HttpUrl

/**
 * Created by mike on 08/06/2018
 */
class RestServiceModule : CoreRestServiceModule() {
  companion object {
    private val BASE_URL = App.context.resources.getString(R.string.base_url)
  }

  @Suppress("UnsafeCallOnNullableType")
  override fun provideBaseUrl(context: Context): HttpUrl = HttpUrl.parse(BASE_URL)!!
}