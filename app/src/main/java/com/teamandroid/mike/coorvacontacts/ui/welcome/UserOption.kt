package com.teamandroid.mike.coorvacontacts.ui.welcome

import android.support.annotation.StringRes
import com.teamandroid.mike.coorvacontacts.App
import com.teamandroid.mike.coorvacontacts.R

/**
 * Created by mike on 12/07/2018
 */
enum class UserOption(@StringRes private val description: Int) {
  NAME(R.string.name),
  LAST_NAME(R.string.lastName),
  EMAIL(R.string.email),
  ;

  override fun toString(): String {
    return App.context.getString(description)
  }
}