package com.teamandroid.mike.coorvacontacts.ui.profile

import android.content.Intent
import android.widget.Toast
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.teamandroid.mike.coorvacontacts.R
import com.teamandroid.mike.coorvacontacts.helper.extensions.load
import com.teamandroid.mike.coorvacontacts.ui.base.CoorvaContactsFragment
import com.teamandroid.mike.coorvacontacts.ui.welcome.CoorvaContactsViewModel
import com.teamandroid.mike.coorvacontacts.vo.User
import com.xmartlabs.bigbang.core.helper.ui.CircleTransform
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by mike on 12/07/2018
 */
@FragmentWithArgs
class UserProfileFragment : CoorvaContactsFragment() {
  companion object {
    const val SHARE_TYPE = "text/plain"
  }

  @Arg
  lateinit var userId: String

  @Inject
  lateinit var viewModel: CoorvaContactsViewModel

  private val disposable = CompositeDisposable()

  override val layoutResId = R.layout.fragment_profile

  override fun onStart() {
    super.onStart()
    // Subscribe to the emissions of the user
    // Update the user name text informatoion on every emission.
    // If we modify the user in a further screen, it ui will be updated automatically
    // In case of error, shows a toast.
    disposable.add(viewModel.getUserWithId(userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({ fillUserData(it) },
            { error ->
              Timber.d(error)
              Toast.makeText(activity, getString(R.string.unable_to_get_contact_profile),
                  Toast.LENGTH_SHORT).show()
            }))
    share_contact_button.setOnClickListener { handleShare() }
  }

  private fun handleShare() {
    val share1 = Intent(Intent.ACTION_SEND)
    share1.type = SHARE_TYPE
    share1.putExtra(Intent.EXTRA_TEXT,
        getString(R.string.share_contact_formatted_string,
            this.profile_contact_name.text,
            this.profile_contact_last_name.text,
            this.profile_contact_email.text,
            this.profile_contact_phone.text))

    //Checking that the user has something to open this kind of intents
    share1.resolveActivity(activity?.packageManager)?.run {
      startActivity(Intent.createChooser(share1, getString(R.string.share_contact)))
    }
  }

  private fun fillUserData(it: User) {
    it.image?.let { image ->
      loadAvatar(image)
    }
    this.profile_contact_name.text = it.firstName
    this.profile_contact_last_name.text = it.lastName
    this.profile_contact_email.text = it.email
    this.profile_contact_phone.text = it.phone
  }

  override fun onStop() {
    super.onStop()
    // clear all the subscription
    disposable.clear()
  }

  private fun loadAvatar(url: String) =
      this.profile_user_thumbnail.load(url, R.drawable.ic_android_coorva_24dp) { transform(CircleTransform()) }
}