package com.teamandroid.mike.coorvacontacts.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.teamandroid.mike.coorvacontacts.vo.User

/**
 * Main database description.
 */
@Database(
    entities = [
      User::class
    ],
    version = 1,
    exportSchema = false
)
abstract class CoorvaContactsDb : RoomDatabase() {

  abstract fun userDao(): UserDao

}