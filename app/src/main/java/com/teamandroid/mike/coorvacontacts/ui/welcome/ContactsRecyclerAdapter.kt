package com.teamandroid.mike.coorvacontacts.ui.welcome

import android.view.ViewGroup
import com.teamandroid.mike.coorvacontacts.R
import com.teamandroid.mike.coorvacontacts.vo.User
import com.xmartlabs.bigbang.ui.common.recyclerview.SingleItemBaseRecyclerViewAdapter

/**
 * Created by mike on 12/07/2018
 */
class ContactsRecyclerAdapter(
    private var userClicked: ((User) -> Unit)? = null
) : SingleItemBaseRecyclerViewAdapter<User, ContactItemView>() {
  override fun onCreateViewHolder(parent: ViewGroup) =
      ContactItemView(inflateView(parent, R.layout.item_contact), userClicked)

  var customersList: List<User>? = null

  fun setCustomers(customers: List<User>) {
    customersList = customers
    setItems(customers)
  }

  fun setCustomersSorted(customers: List<User>, sortedBy: UserOption) {
    customersList = customers
    sortItemsBy(sortedBy)
    setItems(customers)
  }

  fun sortCurrentCustomers(sortedBy: UserOption) {
    val sortedItems = sortItemsBy(sortedBy)
    customersList = sortedItems
    customersList?.let {
      setItems(it)
    }
  }

  fun sortItemsBy(userOption: UserOption): List<User>? {
    return when (userOption) {
      UserOption.NAME -> customersList?.sortedBy { it.firstName }
      UserOption.LAST_NAME -> customersList?.sortedBy { it.lastName }
      UserOption.EMAIL -> customersList?.sortedBy { it.email }
    }
  }
}