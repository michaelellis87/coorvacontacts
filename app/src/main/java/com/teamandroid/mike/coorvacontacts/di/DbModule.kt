package com.teamandroid.mike.coorvacontacts.di

import android.app.Application
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.teamandroid.mike.coorvacontacts.db.CoorvaContactsDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by mike on 08/06/2018
 */
@Module
class DbModule {
  @Singleton
  @Provides
  fun provideDb(app: Application) = Room
      .databaseBuilder(app, CoorvaContactsDb::class.java, "daily_motion.db")
      .fallbackToDestructiveMigration()
      .build()

  @Singleton
  @Provides
  fun provideRoomDb(db: CoorvaContactsDb): RoomDatabase = db

  @Singleton
  @Provides
  fun provideUserDao(db: CoorvaContactsDb) = db.userDao()
}