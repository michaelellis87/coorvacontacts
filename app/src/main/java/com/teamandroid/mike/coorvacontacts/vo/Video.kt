package com.teamandroid.mike.coorvacontacts.vo

import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by mike on 27/06/2018
 */
@Entity(tableName = "videos", primaryKeys = ["id"])
data class Video(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("title")
    val title: String,
    @field:SerializedName("owner")
    val ownerId: String,
    @field:SerializedName("owner.screenname")
    val ownerScreenName: String,
    @field:SerializedName("owner.url")
    val ownerUrl: String,
    @field:SerializedName("thumbnail_360_url")
    val thumbnailUrl: String,
    @field:SerializedName("views_total")
    val viewsCount: Long
)