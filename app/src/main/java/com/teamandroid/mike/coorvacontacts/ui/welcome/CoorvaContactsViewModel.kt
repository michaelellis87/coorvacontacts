package com.teamandroid.mike.coorvacontacts.ui.welcome

import android.arch.lifecycle.ViewModel
import com.teamandroid.mike.coorvacontacts.repository.user.UserRepository
import com.teamandroid.mike.coorvacontacts.vo.User
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by mike on 12/07/2018
 */
class CoorvaContactsViewModel @Inject constructor(private val repository: UserRepository) : ViewModel() {
  /**
   * Get the fetched users list.
   * @return a [Flowable] that will emit every time the fetched users changed.
   */
  // for every emission get the users list
  fun observeUsers(): Flowable<List<User>> {
    return repository.observeFetchedContacts()
  }

  fun fetchUsers(): Completable {
    return repository.fetchContacts()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
  }

  fun getUserWithId(userId: String): Flowable<User> {
    return repository.getUserFromDB(userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
  }

  fun searchBy(input: String, userOption: UserOption): Completable {
    return repository.searchContacts(input, userOption)
        .doOnError { error -> error.message }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
  }
}