package com.teamandroid.mike.coorvacontacts.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.teamandroid.mike.coorvacontacts.ui.welcome.CoorvaContactsViewModel
import com.teamandroid.mike.coorvacontacts.viewmodel.CoorvaContactsViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by mike on 24/05/2018
 */
@Suppress("unused")
@Module
abstract class ViewModelModule {
  @Binds
  @IntoMap
  @ViewModelKey(CoorvaContactsViewModel::class)
  abstract fun bindLoginViewModel(contactsViewModel: CoorvaContactsViewModel): ViewModel

  @Binds
  abstract fun bindViewModelFactory(factory: CoorvaContactsViewModelFactory): ViewModelProvider.Factory
}