package com.teamandroid.mike.coorvacontacts.di

import com.teamandroid.mike.coorvacontacts.helper.common.BuildInfo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import com.xmartlabs.bigbang.core.model.BuildInfo as CoreBuildInfo

/**
 * Created by mike on 24/05/2018
 */
@Module
class AppModule {
  @Provides
  @Singleton
  fun provideBuildInformation(coreBuildInfo: BuildInfo): CoreBuildInfo = coreBuildInfo
}