package com.teamandroid.mike.coorvacontacts.vo

import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by mike on 24/05/2018
 */
@Entity(primaryKeys = ["id"])
data class User(
  val id: String,
  val firstName: String?,
  val lastName: String?,
  val email: String?,
  val phone: String?,
  val image: String?
)