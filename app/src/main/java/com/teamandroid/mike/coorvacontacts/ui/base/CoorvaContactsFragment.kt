package com.teamandroid.mike.coorvacontacts.ui.base

import android.support.annotation.StringRes
import com.afollestad.materialdialogs.MaterialDialog
import com.tbruyelle.rxpermissions2.RxPermissions
import com.teamandroid.mike.coorvacontacts.R
import com.teamandroid.mike.coorvacontacts.ui.common.LoadingView
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.FragmentEvent
import com.xmartlabs.bigbang.core.extensions.observeOnMain
import com.xmartlabs.bigbang.ui.BaseFragment
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.exceptions.CompositeException
import java.io.IOException
import java.util.concurrent.CancellationException

/**
 * Created by mike on 08/05/2018
 */
abstract class CoorvaContactsFragment : BaseFragment() {
  val isViewAlive: Boolean
    get() = isAdded && activity != null

  private val loadingView by lazy {
    context?.let { LoadingView(it) }
  }

  fun showError(message: Int, title: Int = 0, buttonTitle: Int = 0) {
    if (isViewAlive) {
      context?.let {
        MaterialDialog.Builder(it)
            .title(title)
            .content(message)
            .positiveText(buttonTitle)
            .build()
            .show()
      }
    }
  }

  fun showError(error: Throwable, @StringRes message: Int?) {
    if (error is CancellationException) {
      return
    }

    var receiverError = error
    if (receiverError is CompositeException) {
      receiverError = receiverError.exceptions[0]
    }

    when {
      receiverError is IOException -> showConnectionError()
      message == null -> showError(R.string.error_service_call_generic)
      else -> showError(message)
    }
  }

  private fun showConnectionError() {
    showError(R.string.check_your_internet_connection, R.string.no_internet_connection)
  }

  fun showError(error: Throwable, message: String?, title: String) {
    message?.let {
      if (isViewAlive) {
        context?.let {
          MaterialDialog.Builder(it)
              .title(title)
              .content(message)
              .positiveText(android.R.string.ok)
              .build()
              .show()
        }
      }
    }
  }

  fun showLoading(loading: Boolean) {
    if (loading) {
      loadingView?.show()
    } else {
      loadingView?.dismiss()
    }
  }

  fun <T : Any> keepAliveWhileVisible(source: Observable<T>) =
      source.compose(RxLifecycle.bindUntilEvent(lifecycle(), FragmentEvent.DESTROY_VIEW)).observeOnMain()

  fun <T : Any> keepAliveWhileVisible(source: Single<T>) =
      source.compose(RxLifecycle.bindUntilEvent(lifecycle(), FragmentEvent.DESTROY_VIEW)).observeOnMain()

  fun <T : Any> keepAliveWhileVisible(source: Flowable<T>) =
      source.compose(RxLifecycle.bindUntilEvent(lifecycle(), FragmentEvent.DESTROY_VIEW)).observeOnMain()

  fun <T : Any> keepAliveWhileVisible(source: Maybe<T>) =
      source.compose(RxLifecycle.bindUntilEvent(lifecycle(), FragmentEvent.DESTROY_VIEW)).observeOnMain()

  fun keepAliveWhileVisible(source: Completable) =
      source.compose(RxLifecycle.bindUntilEvent<FragmentEvent, FragmentEvent>(lifecycle(), FragmentEvent.DESTROY_VIEW))
          .observeOnMain()
}

fun <T : Any> Observable<T>.prepareForSubscription(view: CoorvaContactsFragment) = view.keepAliveWhileVisible(this)
fun <T : Any> Single<T>.prepareForSubscription(view: CoorvaContactsFragment) = view.keepAliveWhileVisible(this)
fun <T : Any> Flowable<T>.prepareForSubscription(view: CoorvaContactsFragment) = view.keepAliveWhileVisible(this)
fun <T : Any> Maybe<T>.prepareForSubscription(view: CoorvaContactsFragment) = view.keepAliveWhileVisible(this)
fun Completable.prepareForSubscription(view: CoorvaContactsFragment) = view.keepAliveWhileVisible(this)
