package com.teamandroid.mike.coorvacontacts.ui.welcome

import com.f2prateek.dart.HensonNavigable
import com.teamandroid.mike.coorvacontacts.ui.base.CoorvaContactsSingleFragmentActivity
import com.xmartlabs.bigbang.ui.BaseFragment

@HensonNavigable
class WelcomeActivity : CoorvaContactsSingleFragmentActivity() {
  override fun createFragment(): BaseFragment = WelcomeFragmentBuilder().build()
}