package com.teamandroid.mike.coorvacontacts.controller

import com.xmartlabs.bigbang.core.controller.Controller
import com.teamandroid.mike.coorvacontacts.service.AuthService
import javax.inject.Inject

/**
 * Created by mike on 22/06/2018
 */
open class AuthController @Inject constructor(
    private val authService: AuthService,
    private val sessionController: SessionController
) : Controller() {
  //TODO: Do signing things here
}