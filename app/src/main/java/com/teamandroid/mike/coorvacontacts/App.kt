package com.teamandroid.mike.coorvacontacts

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Build
import android.support.annotation.VisibleForTesting
import android.support.multidex.MultiDex
import com.teamandroid.mike.coorvacontacts.di.AppComponent
import com.teamandroid.mike.coorvacontacts.di.DaggerAppComponent
import com.teamandroid.mike.coorvacontacts.di.OkHttpModule
import com.teamandroid.mike.coorvacontacts.di.RestServiceModule
import com.teamandroid.mike.coorvacontacts.di.ServiceGsonModule
import com.teamandroid.mike.coorvacontacts.helper.common.BuildInfo
import com.xmartlabs.bigbang.core.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject
import dagger.internal.Beta

/**
 * Created by mike on 24/05/2018
 */
@Beta
class App : Application(), HasActivityInjector {
  companion object {
    @JvmStatic
    @Suppress("LateinitUsage")
    lateinit var context: App
  }

  @Inject
  lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
  @Inject
  internal lateinit var buildInfo: BuildInfo
  @Suppress("LateinitUsage")
  internal lateinit var applicationComponent : AppComponent

  init {
    @Suppress("LeakingThis")
    context = this
  }

  override fun attachBaseContext(base: Context) {
    super.attachBaseContext(base)

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP || !BuildConfig.DEBUG) {
      MultiDex.install(this)
    }
  }

  override fun onCreate() {
    super.onCreate()
    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }
    initializeInjections()
  }

  private fun initializeInjections() {
    applicationComponent = createComponent()
    applicationComponent.inject(this)
    AppInjector.init(this)
  }

  @VisibleForTesting
  protected fun createComponent(): AppComponent = DaggerAppComponent.builder()
      .application(this)
      .buildInfo(BuildInfo())
      .okHttpModule(OkHttpModule())
      .restServiceGsonModule(ServiceGsonModule())
      .restServiceModule(RestServiceModule())
      .build()

  override fun activityInjector() = dispatchingAndroidInjector
}