package com.teamandroid.mike.coorvacontacts.ui.base

import android.content.Context
import com.xmartlabs.bigbang.ui.SingleFragmentActivity
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class CoorvaContactsSingleFragmentActivity : SingleFragmentActivity() {
  override fun attachBaseContext(newBase: Context?) = super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
}