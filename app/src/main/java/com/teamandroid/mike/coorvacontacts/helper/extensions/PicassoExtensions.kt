package com.teamandroid.mike.coorvacontacts.helper.extensions

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import com.xmartlabs.bigbang.core.extensions.unless

/**
 * Created by mike on 12/07/2018
 */
val Context.picasso: Picasso
  get() = Picasso.with(this)

fun ImageView.load(path: String, placeholderRes: Int?, request: (RequestCreator.() -> RequestCreator) = { this }) =
    unless(path.isBlank()) {
      val requestBuilder = context.picasso.load(path)

      if (placeholderRes != null) {
        requestBuilder.placeholder(placeholderRes)
      } else {
        requestBuilder.noPlaceholder()
      }

      request(requestBuilder).into(this)
    }