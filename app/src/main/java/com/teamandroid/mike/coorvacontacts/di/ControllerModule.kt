package com.teamandroid.mike.coorvacontacts.di

import com.teamandroid.mike.coorvacontacts.controller.SessionController
import com.xmartlabs.bigbang.core.controller.CoreSessionController
import com.xmartlabs.bigbang.core.controller.SharedPreferencesController
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by mike on 12/07/2018
 */
@Module
class ControllerModule {
  @Provides
  @Singleton
  internal fun provideSessionController(sharedPreferencesController: SharedPreferencesController)
      = SessionController(sharedPreferencesController)

  @Provides
  @Singleton
  internal fun provideCoreSessionController(sessionController: SessionController): CoreSessionController
      = sessionController
}